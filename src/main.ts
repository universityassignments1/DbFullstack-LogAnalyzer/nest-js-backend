import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
    const app = await NestFactory.create(AppModule, { cors: true });

    //Make validators in the dto throw errors
    app.useGlobalPipes(
        new ValidationPipe({
            whitelist: true, //Ignore unspecified in the dto fields from requests
        }),
    );
    await app.listen(3333);
}
bootstrap();
