import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { ConfigModule } from '@nestjs/config';
import { FileParserModule } from './file-parser/file-parser.module';
import { FileOutputModule } from './file-output/file-output.module';
import { IbmDbModule } from './ibm-db/ibm-db.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true,
        }),
        AuthModule,
        UserModule,
        FileParserModule,
        FileOutputModule,
        IbmDbModule,
    ],
})
export class AppModule {}
