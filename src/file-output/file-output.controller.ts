import { Controller, Get, HttpCode, HttpStatus, Query, UseGuards, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from '../auth/decorator';
import { GetResultsDto } from './dto';
import { FileOutputService } from './file-output.service';
import { User } from '../auth/user-query.service';

@Controller('file-output')
export class FileOutputController {
    constructor(private fileOutputService: FileOutputService) {}

    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuard('jwt'))
    @Get('results')
    getResults(
        @Query(
            new ValidationPipe({
                transform: true,
                transformOptions: { enableImplicitConversion: true },
                forbidNonWhitelisted: true,
            }),
        )
        dto: GetResultsDto,
        @GetUser() user: User,
    ) {
        return this.fileOutputService.returnResults(dto, user);
    }
}
