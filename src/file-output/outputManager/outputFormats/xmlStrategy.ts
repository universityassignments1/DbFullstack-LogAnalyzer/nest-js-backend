import { OutputFormatStrategy } from '../OutputManager.class';

export class XmlStrategy implements OutputFormatStrategy {
    public convert(strObj: string): string {
        const parsed = JSON.parse(strObj);
        return this.OBJtoXML(parsed);
    }

    private OBJtoXML(obj) {
        var xml = '';
        for (var prop in obj) {
            xml += obj[prop] instanceof Array ? '' : '<' + prop + '>';
            if (obj[prop] instanceof Array) {
                for (var array in obj[prop]) {
                    xml += '<' + prop + '>';
                    xml += this.OBJtoXML(new Object(obj[prop][array]));
                    xml += '</' + prop + '>';
                }
            } else if (typeof obj[prop] == 'object') {
                xml += this.OBJtoXML(new Object(obj[prop]));
            } else {
                xml += obj[prop];
            }
            xml += obj[prop] instanceof Array ? '' : '</' + prop + '>';
        }
        var xml = xml.replace(/<\/?[0-9]{1,}>/g, '');
        return xml;
    }
}
