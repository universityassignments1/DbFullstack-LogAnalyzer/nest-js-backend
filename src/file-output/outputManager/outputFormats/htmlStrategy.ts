import { OutputFormatStrategy } from '../OutputManager.class';

export class HtmlStrategy implements OutputFormatStrategy {
    public convert(strObj: string): string {
        const parsed = JSON.parse(strObj);
        let objKeys = Object.keys(parsed);
        let tableRows = '';
        for (let key of objKeys) {
            tableRows += `<tr><td>${key}</td><td>${parsed[key]}</td></tr>`;
        }

        return `<table><tr><th>Users</th><th>Time spent in seconds</th></tr>${tableRows}</table>`;
    }
}
