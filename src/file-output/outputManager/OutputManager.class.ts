import { BadRequestException } from '@nestjs/common';
import { HtmlStrategy } from './outputFormats/htmlStrategy';
import { JsonStrategy } from './outputFormats/jsonStrategy';
import { XmlStrategy } from './outputFormats/xmlStrategy';

export interface OutputFormatStrategy {
    convert(strObj: string): string;
}

export class OutputManager {
    private _formatStrategy: OutputFormatStrategy;

    public setFormatStrategy(format: string) {
        switch (format) {
            case 'json':
                this._formatStrategy = new JsonStrategy();
                break;
            case 'html':
                this._formatStrategy = new HtmlStrategy();
                break;
            case 'xml':
                this._formatStrategy = new XmlStrategy();
                break;
            default:
                throw new BadRequestException('Invalid format');
        }
    }

    public getResult(strObj: string) {
        return this._formatStrategy.convert(strObj);
    }
}
