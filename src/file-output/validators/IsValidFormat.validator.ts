import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from 'class-validator';

const ALLOWED_FORMATS = ['json', 'html', 'xml'];

@ValidatorConstraint({ name: 'customText', async: false })
export class IsValidFormat implements ValidatorConstraintInterface {
    validate(text: string, args: ValidationArguments): boolean {
        return Boolean(ALLOWED_FORMATS.find((format) => format === text));
    }

    defaultMessage(args: ValidationArguments) {
        // here you can provide default error message if validation failed
        return `Format ($value) is unsupported! Supported formats are ${ALLOWED_FORMATS.join(', ')}`;
    }
}
