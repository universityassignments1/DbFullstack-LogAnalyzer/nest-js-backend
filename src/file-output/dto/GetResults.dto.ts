import { IsNotEmpty, IsString, Validate } from 'class-validator';
import { IsValidFormat } from '../validators/isValidFormat.validator';

export class GetResultsDto {
    @IsNotEmpty()
    @IsString()
    hash: string;

    @IsNotEmpty()
    @IsString()
    @Validate(IsValidFormat)
    format: string;
}
