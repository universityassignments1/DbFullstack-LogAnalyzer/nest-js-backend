import { Injectable, NotFoundException } from '@nestjs/common';
import { GetResultsDto } from './dto';
import { OutputManager } from './outputManager';
import { LogFile, LogFileQueryService } from '../ibm-db/logfile-query.service';
import { User } from '../auth/user-query.service';

@Injectable()
export class FileOutputService {
    constructor(private readonly logfileService: LogFileQueryService) {}

    async returnResults(dto: GetResultsDto, user: User) {
        let logFile: LogFile;
        try {
            logFile = await this.logfileService.findFirst(dto.hash, user.id);
        } catch (error) {
            throw new NotFoundException('File doesn`t exist or you are unauthorized to view it');
        }

        const outputManager = new OutputManager();
        outputManager.setFormatStrategy(dto.format);

        return {
            result: outputManager.getResult(logFile.content),
        };
    }
}
