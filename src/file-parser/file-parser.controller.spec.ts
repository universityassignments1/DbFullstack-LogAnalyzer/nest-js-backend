import { Test, TestingModule } from '@nestjs/testing';
import { FileParserController } from './file-parser.controller';
import * as pactum from 'pactum';
import { FileParserService } from './file-parser.service';
import { FileParserFactory } from './FileParser';
import { ConfigService } from '@nestjs/config';
import { AuthDto } from '../auth/dto';

describe('FileParserController', () => {
	let controller: FileParserController;

	beforeAll(async () => {
		pactum.request.setBaseUrl('http://localhost:3333');
	});

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			controllers: [FileParserController],
            providers: [FileParserService, FileParserFactory, ConfigService],
		}).compile();

		controller = module.get<FileParserController>(FileParserController);
	});

	it('should be defined', () => {
		expect(controller).toBeDefined();
	});

	describe('upload-file', () => {
        const testDto = {
            gracePeriod: 60,
        }
        const correctfilePath = './src/file-parser/testFiles/withLogout.txt';
        const invalidFiePath = './src/file-parser/testFiles/invalidFormat.png';

        const testdto: AuthDto = {
          email: 'test@mail.com',
          password: '123'
        };

        it('signin', () => {
            return pactum.spec().post('/auth/signup').withBody(testdto)
            .stores('access_token', 'accessToken'); 
        });
        
        it('signup', () => {
          return pactum.spec().post('/auth/signin').withBody(testdto)
          .stores('access_token', 'accessToken');
        });

		describe("gracePeriod input", () => {
            it('should throw if gracePeriod is missing', () => {
                return pactum.spec().post('/file-parser/upload-file').withHeaders({Authorization: 'Bearer $S{access_token}'})
                .withFile(correctfilePath).expectStatus(400);
            });

            it('should throw if gracePeriod is not a number', () => {
                return pactum.spec().post('/file-parser/upload-file').withHeaders({Authorization: 'Bearer $S{access_token}'})
                .withBody({gracePeriod: 'ala'}).expectStatus(400);
            });

            it('should throw if gracePeriod is not a number', () => {
                return pactum.spec().post('/file-parser/upload-file').withHeaders({Authorization: 'Bearer $S{access_token}'})
                .withBody({gracePeriod: 'ala'}).expectStatus(400);
            });

            it('should throw if gracePeriod is negative', () => {
                return pactum.spec().post('/file-parser/upload-file').withHeaders({Authorization: 'Bearer $S{access_token}'})
                .withBody({gracePeriod: -1}).expectStatus(400);
            });

            it('should throw if gracePeriod is negative', () => {
                return pactum.spec().post('/file-parser/upload-file').withHeaders({Authorization: 'Bearer $S{access_token}'})
                .withBody({gracePeriod: -1}).expectStatus(400);
            });
        });

        describe('files input', () => {
            it('should throw if file is missing', () => {
                return pactum.spec().post('/file-parser/upload-file').withHeaders({Authorization: 'Bearer $S{access_token}'})
                .withBody(testDto).expectStatus(400);
            });

            it('should throw if file is incorrect format', () => {
                return pactum.spec().post('/file-parser/upload-file').withHeaders({Authorization: 'Bearer $S{access_token}'})
                .withBody(testDto).withFile(invalidFiePath).expectStatus(400);
            });

            it('should throw if file one of the files is incorrect format', () => {
                return pactum.spec().post('/file-parser/upload-file').withHeaders({Authorization: 'Bearer $S{access_token}'})
                .withBody(testDto).withFile(correctfilePath).withFile(invalidFiePath).expectStatus(400);
            });

            it('should throw if file one of the files is incorrect format v2', () => {
                return pactum.spec().post('/file-parser/upload-file').withHeaders({Authorization: 'Bearer $S{access_token}'})
                .withBody(testDto).withFile(correctfilePath).withFile(invalidFiePath).withFile(invalidFiePath).expectStatus(400);
            });
        });
	});
});
