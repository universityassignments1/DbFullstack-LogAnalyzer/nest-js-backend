import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { FileParserController } from './file-parser.controller';
import { FileParserService } from './file-parser.service';
import { FileParserFactory, ParsedFile } from './FileParser';
const fs = require('fs');

function readLocalFile(path: string): string {
    try {
        const data = fs.readFileSync(path, 'utf8');
        return data;
    } catch (err) {
        console.error(err);
    }
}

describe('FileParserService', () => {
    let service: FileParserService;
    let fileParser;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [FileParserController],
            providers: [FileParserService, FileParserFactory, ConfigService],
        }).compile();

        service = module.get<FileParserService>(FileParserService);

        fileParser = FileParserFactory.getFileParser('.txt', 60);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    describe('TxtFileParser', () => {

        describe('parse', () => {
            it('should throw if date is invalid', () => {
                let file = '6373hffjb john login \n 3738393836 peter login';
                expect(() => fileParser.parse(file)).toThrow();
            });

            it('should throw if format is fucked up', () => {
                let file = readLocalFile('./src/file-parser/testFiles/messedFormat.txt');
                expect(() => fileParser.parse(file)).toThrowError('Cannot parse file. Format should be <timestamp> <username> <action>'); 
            });

            it('should throw if timestamps are not linear', () => {
                let file = readLocalFile('./src/file-parser/testFiles/nonLinearTimestamps.txt');
                expect(() => fileParser.parse(file)).toThrowError(); 
            });

            it('should throw if the file doen`t follow the format', () => {
                let file = readLocalFile('./src/file-parser/testFiles/invalidFileContents.txt');
                expect(() => fileParser.parse(file)).toThrowError(); 
            });

            it('test with simple file', () => {
                let file = '1646123779 john_smith login\n1646123805 john_smith logout\n';

                let expectedResult: ParsedFile = {
                    john_smith: 26,
                }

                const result = fileParser.parse(file);
                expect(result).toStrictEqual<ParsedFile>(expectedResult);
            });

            it('returns correct object with provided both login and logout logs', () => {
                let file = readLocalFile('./src/file-parser/testFiles/withLogout.txt');

                const correctResult: ParsedFile = {
                    john_smith: 26,
                    peter_johnson: 600,
                }
                const result = fileParser.parse(file);
                expect(result).toStrictEqual<ParsedFile>(correctResult);
            });

            it('test without logout logs with simple file', () => {
                let file = '1646123779 john_smith login\n';

                let expectedResult: ParsedFile = {
                    john_smith: 60,
                }

                const result = fileParser.parse(file);
                expect(result).toStrictEqual<ParsedFile>(expectedResult);
            });

            it('returns correct object with provided only login logs', () => {
                let file = readLocalFile('./src/file-parser/testFiles/withoutLogout.txt');

                const correctResult: ParsedFile = {
                    john_smith: 128,
                    peter_johnson: 70,
                }
                const result = fileParser.parse(file);
                expect(result).toStrictEqual<ParsedFile>(correctResult);
            });

            it('returns correct object with combined logs', () => {
                let file = `1646123779 john_smith login
                            1646123785 john_smith check e-mail
                            1646123798 peter_johnson login
                            1646123798 niki login
                            1646123805 john_smith login
                            1646123806 peter_johnson check e-mail
                            1646123807 john_smith delete e-mail message 1221387
                            1646123819 niki open pornHub
                            1646123807 peter_johnson check e-mail
                            1646123808 peter_johnson delete e-mail
                            1646124419 niki logout
                            1646124419 joro login`;

                const correctResult: ParsedFile = {
                    john_smith: 128,
                    peter_johnson: 70,
                    niki: 621,
                    joro: 60,
                }
                const result = fileParser.parse(file);
                expect(result).toStrictEqual<ParsedFile>(correctResult);
            });

            it('handles multiple files', () => {
                let file2 = readLocalFile('./src/file-parser/testFiles/withLogout.txt');
                let file1 = readLocalFile('./src/file-parser/testFiles/withoutLogout.txt');
                let file0 = '1646123819 bob login\n';

                const correctResult: ParsedFile = {
                    john_smith: 154,
                    peter_johnson: 670,
                    bob: 60,
                }
                let strArr = new Array<string>();
                strArr.push(file2, file1, file0);

                const result = fileParser.parse(strArr);
                expect(result).toStrictEqual<ParsedFile>(correctResult);
            });
        });
    });
});
