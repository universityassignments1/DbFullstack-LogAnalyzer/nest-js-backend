import { UnsupportedMediaTypeException } from "@nestjs/common";
import { TxtFileParser } from "./TxtFileParser.class";

export interface User {
    lastTimeLoggedIn: number;
    secondsElapsed: number;
}

export interface ParsedFile {
    [userName: string]: number;
}

const SupportedFormats = {
    TXT: '.txt',
}

export interface FileParser {
    parse(inputFileStream: string | string[]): ParsedFile;
    get parsedFile(): ParsedFile;
}


/**
 *The FileParserFactory class is a factory class that returns a FileParser object based on the file format.
 *
 * @class FileParserFactory
 */
export class FileParserFactory {
    /**
     * "This function returns a FileParser object based on the file format passed in."
     * 
     * @param {string} fileFormat - The format of the file that is being uploaded.
     * @param {number} gracePeriod - number -&gt; The grace period in seconds
     * @returns A FileParser object.
     */
    public static getFileParser(fileFormat: string, gracePeriod: number): FileParser {
        switch(fileFormat) {
            case SupportedFormats.TXT:
                return new TxtFileParser(gracePeriod);
            
            default:
                throw new UnsupportedMediaTypeException("Unsupported file format");
        }
    }
}