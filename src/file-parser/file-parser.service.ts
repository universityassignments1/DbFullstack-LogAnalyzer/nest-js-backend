import { BadRequestException, Injectable } from '@nestjs/common';
import * as path from 'path';
import { FileParserFactory } from './FileParser';
import { LogFile, LogFileQueryService } from '../ibm-db/logfile-query.service';
import { User } from '../auth/user-query.service';

@Injectable()
export class FileParserService {
    constructor(private logfileQueryService: LogFileQueryService) {}

    async fileParse(files: Array<Express.Multer.File>, gracePeriod: number, user: User) {
        const fileNames = files.map((file) => file.originalname);
        const hash = hashFileNames(fileNames);

        //Search if this files were already parsed
        let logFile: LogFile;
        try {
            logFile = await this.logfileQueryService.findFirst(hash, user.id);
        } catch (error) {
            try {
                logFile = await this.uploadFile(files, gracePeriod, user, hash, fileNames);
            } catch (error) {
                throw new BadRequestException('Error uploading file');
            }
        }

        return logFile;
    }

    private uploadFile(
        files: Array<Express.Multer.File>,
        gracePeriod: number,
        user: User,
        hash: string,
        fileNames: string[],
    ) {
        const ext = path.extname(files[0].originalname);
        const filesString = files.map((file) => file.buffer.toString());

        const fileParser = FileParserFactory.getFileParser(ext, Number(gracePeriod));
        const parsedFile = fileParser.parse(filesString);

        return this.logfileQueryService.create(hash, user.id, fileNames.join(), JSON.stringify(parsedFile));
    }
}

function hashFileNames(fileNames: string[]) {
    const sorted = fileNames.sort(); //Sorts so even if the same files are given in different order it will still generate the same hash
    return stringToHash(sorted.join());
}

function stringToHash(string: string): string {
    let hash = 0;

    if (string.length == 0) return String(hash);

    for (let i = 0; i < string.length; i++) {
        const char = string.charCodeAt(i);
        hash = (hash << 5) - hash + char;
        hash = hash & hash;
    }

    return String(hash);
}
