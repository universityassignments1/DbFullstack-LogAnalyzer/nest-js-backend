import {
    BadRequestException,
    Controller,
    HttpCode,
    HttpStatus,
    Post,
    Query,
    UploadedFiles,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FilesInterceptor } from '@nestjs/platform-express';
import { GetUser } from '../auth/decorator';
import { FileParserService } from './file-parser.service';
import { fileTypeFilter } from './multer-options-objects';
import { User } from '../auth/user-query.service';

const MAX_COUNT_FILES = 5;
const MB = 1e6;
const MAX_FILE_SIZE = 5 * MB;

@UseGuards(AuthGuard('jwt')) //Uses the JwtStrategy in auth/strategy to guard this route
@Controller('file-parser')
export class FileParserController {
    constructor(private FileParserService: FileParserService) {}

    @HttpCode(HttpStatus.CREATED)
    @Post('upload-file')
    @UseInterceptors(
        FilesInterceptor('files', MAX_COUNT_FILES, {
            fileFilter: fileTypeFilter,
            limits: { fileSize: MAX_FILE_SIZE },
        }),
    )
    fileParse(
        @UploadedFiles() files: Array<Express.Multer.File>,
        @Query('grace-period') gracePreriod: number,
        @GetUser() user: User,
    ) {
        if (!files) throw new BadRequestException('Should provide atleast 1 valid file');
        return this.FileParserService.fileParse(files, gracePreriod, user);
    }
}
