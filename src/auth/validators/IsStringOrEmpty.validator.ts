import {
    isString,
    ValidatorConstraint,
    ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ name: 'customText', async: false })
export class IsStringOrEmpty implements ValidatorConstraintInterface {
    validate(text: string) {
        if (!text) {
            return true;
        }
        return isString(text);
    }

    defaultMessage() {
        // here you can provide default error message if validation failed
        return `Value ($value) is not a real string or null`;
    }
}
