import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthContoller } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategy';
import { UserQueryService } from './user-query.service';

@Module({
    imports: [JwtModule.register({})], // Sign and decode json web tokens
    controllers: [AuthContoller],
    providers: [AuthService, JwtStrategy, UserQueryService],
})
export class AuthModule {}
