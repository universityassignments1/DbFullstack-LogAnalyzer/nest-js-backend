import { IsEmail, IsNotEmpty, IsString, Validate } from 'class-validator';
import { IsStringOrEmpty } from '../validators/IsStringOrEmpty.validator';

export class AuthDto {
    @IsEmail()
    @IsNotEmpty()
    email: string;

    @IsString()
    @IsNotEmpty()
    password: string;

    @Validate(IsStringOrEmpty)
    firstName?: string;

    @Validate(IsStringOrEmpty)
    lastName?: string;
}
