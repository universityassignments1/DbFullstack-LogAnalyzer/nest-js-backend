import { Injectable } from '@nestjs/common';
import { IbmDbService } from '../ibm-db/ibm-db.service';

export interface User {
    id: number;
    email: string;
    passwordHash: string;
    firstName?: string;
    lastName?: string;
}

@Injectable()
export class UserQueryService {
    constructor(private dbService: IbmDbService) {}

    async createUser(
        email: string,
        hash: string,
        firstName: string,
        lastName: string,
    ): Promise<User> {
        const query = `
            INSERT INTO USER (EMAIL, PASSWORDHASH, FNAME, LNAME)
            VALUES (?, ?, ?, ?)
        `;
        await this.dbService.conn.query(query, [
            email,
            hash,
            firstName || null,
            lastName || null,
        ]);

        return this.getUser(email);
    }

    async getUser(email: string): Promise<User> {
        const query = `
            SELECT * FROM USER WHERE EMAIL = ?
        `;
        const res = await this.dbService.conn.query(query, [email]);

        return {
            id: res[0].ID,
            email: res[0].EMAIL,
            passwordHash: res[0].PASSWORDHASH,
            firstName: res[0].FNAME,
            lastName: res[0].LNAME,
        };
    }
}
