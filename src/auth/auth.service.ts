import {
    BadRequestException,
    ForbiddenException,
    Injectable,
} from '@nestjs/common';
import { AuthDto } from './dto';
import * as argon from 'argon2';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { UserQueryService, User } from './user-query.service';

const JWT_TOKEN_EXPIRATION = '30m'; // After 15 minutes users need to relog in

@Injectable()
export class AuthService {
    constructor(
        private authQueryService: UserQueryService,
        private jwt: JwtService,
        private config: ConfigService,
    ) {}

    async signup(dto: AuthDto) {
        const hash = await argon.hash(dto.password);

        let user: User;
        try {
            user = await this.authQueryService.createUser(
                dto.email,
                hash,
                dto.firstName,
                dto.lastName,
            );
        } catch (error) {
            if (error.sqlcode === -803) {
                throw new ForbiddenException('User already exists');
            }
            throw new ForbiddenException(error);
        }

        return this.signToken(user.id, user.email);
    }

    async signin(dto: AuthDto) {
        let user: User;
        try {
            user = await this.authQueryService.getUser(dto.email);
        } catch (error) {
            throw new BadRequestException('User not found');
        }

        const passwordsMatch = await argon.verify(
            user.passwordHash,
            dto.password,
        );
        if (!passwordsMatch)
            throw new ForbiddenException('Passwords don`t match');

        return this.signToken(user.id, user.email);
    }

    async signToken(
        userId: number,
        email: string,
    ): Promise<{ accessToken: string }> {
        const payload = {
            sub: userId,
            email,
        };

        const secret = this.config.get('JWT_SECRET');

        const token = await this.jwt.signAsync(payload, {
            expiresIn: JWT_TOKEN_EXPIRATION,
            secret: secret,
        });

        return {
            accessToken: token,
        };
    }
}
