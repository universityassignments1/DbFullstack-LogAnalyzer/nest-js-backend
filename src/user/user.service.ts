import { Injectable, NotFoundException } from '@nestjs/common';
import { LogFile, LogFileQueryService } from '../ibm-db/logfile-query.service';
import { User } from '../auth/user-query.service';

@Injectable()
export class UserService {
    constructor(private readonly logfileService: LogFileQueryService) {}

    async getUserFiles(user: User) {
        let logFiles: LogFile[];
        try {
            logFiles = await this.logfileService.findMany(user.id);
        } catch (error) {
            throw new NotFoundException('Either user doesn`t exist or there are no files found');
        }

        logFiles.forEach((file) => {
            delete file.hash;
            delete file.userId;
        });

        return {
            files: logFiles,
        };
    }
}
