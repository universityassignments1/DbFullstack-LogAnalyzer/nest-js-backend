import { Controller, Get, HttpCode, HttpStatus, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from '../auth/decorator';
import { UserService } from './user.service';
import { IbmDbService } from '../ibm-db/ibm-db.service';
import { User } from '../auth/user-query.service';

@UseGuards(AuthGuard('jwt')) //Uses the JwtStrategy in auth/strategy to guard this route
@Controller('users')
export class UserController {
    constructor(private userService: UserService, private dbService: IbmDbService) {}

    @HttpCode(HttpStatus.OK)
    @Get('my-files')
    getMe(@GetUser() req: User) {
        return this.userService.getUserFiles(req);
    }
}
