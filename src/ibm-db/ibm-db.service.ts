import { Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as imdb from 'ibm_db';

@Injectable()
export class IbmDbService implements OnModuleInit, OnModuleDestroy {
    conn: imdb.Database;

    constructor(private config: ConfigService) {}

    onModuleInit() {
        const connectionString = this.config.get('IBM_DB2_URL');
        this.conn = imdb.openSync(connectionString);
    }

    onModuleDestroy() {
        this.conn.closeSync();
    }
}
