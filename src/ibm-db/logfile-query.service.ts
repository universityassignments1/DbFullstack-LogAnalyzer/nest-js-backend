import { Injectable } from '@nestjs/common';
import { IbmDbService } from '../ibm-db/ibm-db.service';

export interface LogFile {
    hash: string;
    userId: number;
    timestamp: string;
    name: string;
    content: string;
}

@Injectable()
export class LogFileQueryService {
    constructor(private dbService: IbmDbService) {}

    async findFirst(hash: string, userId: number): Promise<LogFile> {
        const query = `
            SELECT * FROM LOGFILE WHERE HASH = ? AND USERID = ?
            ORDER BY TIMESTAMP DESC
            FETCH FIRST ROW ONLY
        `;
        const res = await this.dbService.conn.query(query, [hash, userId]);

        return {
            hash: res[0].HASH,
            userId: res[0].USERID,
            timestamp: res[0].TIMESTAMP,
            name: res[0].NAME,
            content: res[0].CONTENT,
        };
    }

    async findMany(userId: number): Promise<LogFile[]> {
        const query = `
            SELECT * FROM LOGFILE WHERE USERID = ?
        `;
        const res = await this.dbService.conn.query(query, [userId]);

        return res.map((row) => ({
            hash: row.HASH,
            userId: row.USERID,
            timestamp: row.TIMESTAMP,
            name: row.NAME,
            content: row.CONTENT,
        }));
    }

    async create(hash: string, userId: number, name: string, content: string): Promise<LogFile> {
        const query = `
            INSERT INTO LOGFILE (HASH, USERID, NAME, CONTENT)
            VALUES (?, ?, ?, ?)
        `;
        await this.dbService.conn.query(query, [hash, userId, name, content]);

        return this.findFirst(hash, userId);
    }
}
