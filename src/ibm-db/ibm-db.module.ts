import { Global, Module } from '@nestjs/common';
import { IbmDbService } from './ibm-db.service';
import { LogFileQueryService } from './logfile-query.service';

@Global()
@Module({
    providers: [IbmDbService, LogFileQueryService],
    exports: [IbmDbService, LogFileQueryService],
})
export class IbmDbModule {}
