set schema FN24_45803;

--PASSWORDHASHES FOR PASSWORDS: 'password' and '123'
INSERT INTO USER (EMAIL, PASSWORDHASH, FNAME, LNAME) VALUES
('chechen@mail.com', '$argon2id$v=19$m=4096,t=3,p=1$AgdtAGDM3Lvt0h0hkKvvBQ$kMlzEhEei2gY1LCzOhbsZIprgXcSB385X6F07HLClQQ', 'Checheneca', 'Chuka'),
('test@mail.bg', '$argon2id$v=19$m=4096,t=3,p=1$QLegeNBT+nMoYZEGtgiIRA$kNu2MSYcEeGKgxO2US4lVMQqQSo4IxMC/NlGAMsRMP0', 'Glavniya', 'Iliev');

-- hash for filename 'simple.txt'
INSERT INTO LOGFILE (HASH, USERID, NAME, CONTENT) VALUES
('-1432701900', 1, 'simple.txt', '{"john_smith":30,"peter_johnson":30}');

INSERT INTO LOGHISTORY (USERID, FILENAME) VALUES
(1, 'simple.txt');
