set schema FN24_45803;

-- Function that checks if a user with given password hash exist in the db
-- Returns 0 when either the user doesn't exist or the email and password don't match
CREATE OR REPLACE FUNCTION AuthenticateUser(in_email varchar(30), in_passwordhash varchar(100))
RETURNS INT
BEGIN
   DECLARE v_usercount INT;

   SELECT COUNT(USER.EMAIL) INTO v_usercount
    FROM USER
        WHERE USER.EMAIL = in_email AND USER.PASSWORDHASH = in_passwordhash;

   RETURN v_usercount;
END;

--test good path - should return 1
VALUES FN24_45803.AuthenticateUser('chechen@mail.com', '$argon2id$v=19$m=4096,t=3,p=1$AgdtAGDM3Lvt0h0hkKvvBQ$kMlzEhEei2gY1LCzOhbsZIprgXcSB385X6F07HLClQQ');

--test the given user doesn't exist - should return 0
VALUES FN24_45803.AuthenticateUser('kiro.breika@mail.com', '$argon2id$v=19$m=4096,t=3,p=1$AgdtAGDM3Lvt0h0hkKvvBQ$kMlzEhEei2gY1LCzOhbsZIprgXcSB385X6F07HLClQQ');

--test the given credentials don't match - should return 0
VALUES FN24_45803.AuthenticateUser('chechen@mail.com', 'parola');


-- Reruns loghistory of a user in_days back
CREATE OR REPLACE FUNCTION GetUserLatestLogHistory(in_userid INT, in_daysback INT)
RETURNS TABLE(
    TIMESTAMP TIMESTAMP,
    FILENAME VARCHAR(30)
)
RETURN SELECT LOGHISTORY.TIMESTAMP, LOGHISTORY.FILENAME FROM LOGHISTORY
WHERE TIMESTAMP > CURRENT_TIMESTAMP - in_daysback DAYS
AND LOGHISTORY.USERID = in_userid;

SELECT * FROM TABLE(FN24_45803.GetUserLatestLogHistory(1, 100)) T;


